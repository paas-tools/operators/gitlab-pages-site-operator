Gitlab Pages operator
==========================

This operator is responsible for reconciliation of `GitlabPagesSite` CustomResource. The operator's main goal is to generate proper `httpd` configuration for each `GitlabPagesSite` based on its `spec`. For every `GitlabPagesSite` the operator generates `httpd` configuration which is stored in a `Secret` (`Secret`s name is specified via `virtual-hosts-secret-name` command-line argument). Each configuration file (`Secret` key) holds details about a given `VirtualHost` (each `GitlabPagesSite` has a corresponding `VirtualHost` in the `httpd` config) and how to proxy requests to `gitlab-pages` endpoint (which is specified via `gitlab-pages-url` command-line argument).

Reconciliation steps:
1. If `GitlabPagesSite` doesn't have a custom finalizer (`webservices.cern.ch/static-site-finalizer`), then add it and await next reconiliation
2. If `GitlabPagesSite` has been marked for deletion (`GetDeletionTimestamp() != nil`), we need to remove the key from the `Secret` along with the `Route` created specifically for this `GitlabPagesSite`
3. If `GitlabPagesSite` has not been marked for deletion, then we need to verify whether an `ApplicationRegistration` has been created. We requeue the request until `ApplicationRegistration` is ready.
4. Once OIDC-related resource are available, we continue by creating a key in the `Secret` with `httpd` config for the `GitlabPagesSite`.
5. Finally we create a `Route` to expose the `httpd` `Service`. This `Service` points to `Pod`s that run `httpd` containers configured to ingest the configuration stored in the `Secret`. 


## okd4-install Helm values

GitlabPagesSite operator is deployed on the [webEOS cluster](https://webeos.cern.ch/) and the version of it is controlled [via Helm value](https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/chart/charts/gitlab-pages-sites/values.yaml#L7).
/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ConditionTypeGitlabPagesSiteCreated string = "GitlabPagesSiteCreated"

	// Reason used when an ApplicationRegistration is missing
	ConditionReasonMissingApplicationRegistration string = "ApplicationRegistrationMissing"

	// Reason for the Condition in GitlabPagesSite status when site was successfully created
	ConditionReasonSuccessful string = "Successful"

	// Reason for the Condition in GitlabPagesSite status when site was not created successfully
	ConditionReasonFailed string = "Failed"
)

// This is a valid DNS name. OPA will limit further names that can be used.
// NB: a DNS label consisting of only digits is not allowed, but we don't have backtracking in these regex so it's difficult to achieve!

// +kubebuilder:validation:Pattern:="^([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))*$"
type Url string

// +kubebuilder:validation:MinItems=1
type SiteUrls []Url

func (urls SiteUrls) Contains(url string) bool {
	for _, siteUrl := range urls {
		if string(siteUrl) == url {
			return true
		}
	}

	return false
}

func (urls SiteUrls) ToStringSlice() []string {
	siteUrls := []string{}

	for _, url := range urls {
		siteUrls = append(siteUrls, string(url))
	}

	return siteUrls
}

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// Publish a Gitlab Pages site at CERN, optionally with CERN SSO
type GitlabPagesSiteSpec struct {
	// This is a valid DNS name. OPA will limit further names that can be used.
	// NB: a DNS label consisting of only digits is not allowed, but we don't have backtracking in these regex so it's difficult to achieve!

	// Array of custom domains of the Gitlab Pages site
	// +kubebuilder:validation:Required
	Hosts SiteUrls `json:"hosts"`

	// Anonymous specifies whether the Gitlab Pages site should be protected with CERN SSO. If set to True
	// no authentication will be required to access the site.
	// +kubebuilder:validation:Optional
	// +kubebuilder:validation:Enum=true;false
	// +kubebuilder:default:=false
	Anonymous bool `json:"anonymous,omitempty"`
}

// GitlabPagesSiteStatus defines the observed state of GitlabPagesSite
type GitlabPagesSiteStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Conditions represent the latest available observations of an object's state
	Conditions []metav1.Condition `json:"conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// GitlabPagesSite allows to deploy a static site from a properly configured Gitlab repository.
type GitlabPagesSite struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GitlabPagesSiteSpec   `json:"spec,omitempty"`
	Status GitlabPagesSiteStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// GitlabPagesSiteList contains a list of GitlabPagesSite
type GitlabPagesSiteList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GitlabPagesSite `json:"items"`
}

func init() {
	SchemeBuilder.Register(&GitlabPagesSite{}, &GitlabPagesSiteList{})
}

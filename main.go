/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"errors"
	"flag"
	"os"
	"regexp"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	routev1 "github.com/openshift/api/route/v1"

	authzalpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	webservicescernchv1alpha1 "gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/controllers"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(routev1.AddToScheme(scheme))
	utilruntime.Must(webservicescernchv1alpha1.AddToScheme(scheme))
	utilruntime.Must(authzalpha1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	var virtualHostsSecretPrefixName string
	var virtualHostsSecretCount int
	var namespace string
	var gitlabPagesURL string
	var authProxyServiceName string
	var authProxyServicePort int
	var oidcReturnPath string
	var routerShardLabel string
	var defaultRouterShard string
	var sharedSubdomainRegex string
	var domainRegex *regexp.Regexp
	var err error

	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false, "Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&namespace, "namespace", "", "The name of the namespace where this operator is running. "+
		"It will be also used when fetching objects required to proper functioning of the operator (virtual-hosts Secret, auth-proxy Service).")
	flag.StringVar(&virtualHostsSecretPrefixName, "virtual-hosts-secret-prefix-name", "", "The prefix name of the Secrets where httpd configuration should be stored.")
	flag.IntVar(&virtualHostsSecretCount, "virtual-hosts-secret-count", 10, "The number of secrets to be used to store httpd configuration.")
	flag.StringVar(&gitlabPagesURL, "gitlab-pages-url", "", "The URL of the gitlab pages webserver.")
	flag.StringVar(&authProxyServiceName, "auth-proxy-service-name", "", "The name of the service which exposes authenticating proxy.")
	flag.IntVar(&authProxyServicePort, "auth-proxy-service-port", 8080, "The port of the service which exposes authenticating proxy. Routes will use this value as their targetPort.")
	flag.StringVar(&oidcReturnPath, "oidc-return-path", "", "Trailing portion of the OidcReturnURI.")
	flag.StringVar(&routerShardLabel, "router-shard-label", "", "The label added to the GPS's Route.")
	flag.StringVar(&defaultRouterShard, "default-router-shard", "", "The default router shard that every Route should be assigned to.")
	flag.StringVar(&sharedSubdomainRegex, "shared-subdomain-regex", "", "For hosts that don't match this regex the operator will make sure an LE certificate is automatically provisioned.")

	opts := zap.Options{
		Development: true,
	}

	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	// do we even need namespace? we could just default to the namespace where this operator
	// is running (we can retrieve this from /var/run/secrets/kubernetes.io/serviceaccount/namespace)
	if len(namespace) == 0 {
		setupLog.Error(errors.New("missing required arguments"), "-namespace argument is required")
		os.Exit(1)
	}

	if len(virtualHostsSecretPrefixName) == 0 {
		setupLog.Error(errors.New("missing required arguments"), "-virtual-hosts-secret-prefix-name is required")
		os.Exit(1)
	}

	if len(gitlabPagesURL) == 0 {
		setupLog.Error(errors.New("missing required arguments"), "-gitlab-pages-url argument is required")
		os.Exit(1)
	}

	if len(authProxyServiceName) == 0 {
		setupLog.Error(errors.New("missing required arguments"), "-auth-proxy-service-name is required")
		os.Exit(1)
	}

	if len(oidcReturnPath) == 0 {
		setupLog.Error(errors.New("missing required arguments"), "-oidc-return-path is required")
		os.Exit(1)
	}

	if len(sharedSubdomainRegex) != 0 {
		domainRegex, err = regexp.Compile(sharedSubdomainRegex)
		if err != nil {
			setupLog.Error(err, "invalid value of shared-subdomain-regex")
			os.Exit(1)
		}
	}

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	config := ctrl.GetConfigOrDie()
	mgr, err := ctrl.NewManager(config, ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "a09d0557.webservices.cern.ch",
		Namespace:              "",
	})

	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.GitlabPagesSiteReconciler{
		Client:                       mgr.GetClient(),
		Scheme:                       mgr.GetScheme(),
		VirtualHostsSecretPrefixName: virtualHostsSecretPrefixName,
		VirtualHostsSecretCount:      virtualHostsSecretCount,
		Namespace:                    namespace,
		GitlabPagesURL:               gitlabPagesURL,
		AuthProxyServiceName:         authProxyServiceName,
		AuthProxyServicePort:         authProxyServicePort,
		OIDCReturnPath:               oidcReturnPath,
		RouterShardLabel:             routerShardLabel,
		DefaultRouterShard:           defaultRouterShard,
		SharedSubdomainRegex:         domainRegex,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "GitlabPagesSite")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}

package controllers

import (
	"bytes"
	"errors"
	"fmt"
	"text/template"

	"github.com/Masterminds/sprig"
	webservicescernchv1alpha1 "gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
)

const httpdConfigTemplate string = `
{{- range $index, $host := .Hosts }}
<VirtualHost *:8080>
  ServerName {{ $host | quote }}

  ProxyAddHeaders Off
  ProxyPass / {{ $.GitlabPagesURL | quote }}
  ProxyPreserveHost On

  {{- if (not $.Anonymous) }}
  OIDCProviderMetadataURL {{ printf "%s/.well-known/openid-configuration" $.OIDCSecret.IssuerURL | quote }}
  OIDCClientID {{ $.OIDCSecret.ClientID | quote }}
  OIDCClientSecret {{ $.OIDCSecret.ClientSecret | quote }}

  {{- /* OIDCRedirectURI is a vanity URL that must point to a path protected by this module but must NOT point to any content */}}
  OIDCRedirectURI {{ getOIDCReturnUri $host $.OIDCReturnPath | quote }}
  OIDCCryptoPassphrase {{ $.OIDCSecret.SuggestedCookieSecret | quote }}
  OIDCScope "openid email"
  OIDCRemoteUserClaim email
  OIDCSessionType server-cache

  {{- /* Invalidate the session after one day - that is the lifetime of a Keycloak session */}}
  OIDCSessionInactivityTimeout 86400

  {{- /*
    Use files to cache user session state and avoid problems with frequent requests to Keycloak. The default cache type is shm
    which doesn't work well with our custom setup in which httpd processes are frequently recreated (in the event of configuration change
    we send a signal to the master httpd process instructing it to recreate children processes).
  */}}
  OIDCCacheType file

  {{- /* Improvement for the future: mount a PVC at the location of the OIDCCacheDir in the auth-proxy */}}
  OIDCCacheDir /var/cache/httpd/mod_auth_openidc/cache

  {{- /* 
	State (not Session) cookies can pile up if browser requests multiple protected URLs without having authenticated - each one generates its own state.
	This results in HTTP 503 errors.
	Increase number of cookies from 7 to 10 and rotate out old cookies cf. https://github.com/OpenIDC/mod_auth_openidc/wiki/Cookies#state-cookies-are-piling-up
  */}}
  OIDCStateMaxNumberOfCookies 10 true 

  <Location / >
    AuthType openid-connect
    Require valid-user
  </Location>
  {{- end }}
</VirtualHost>
{{- end }}
`

var httpdTemplate *template.Template

func init() {
	httpdTemplate = template.Must(template.New("httpd-config.template").Funcs(sprig.TxtFuncMap()).Funcs(map[string]interface{}{"getOIDCReturnUri": getOIDCReturnUri}).Parse(httpdConfigTemplate))
}

func generateSecretKey(gitlabPagesSite *webservicescernchv1alpha1.GitlabPagesSite) string {
	return fmt.Sprintf("%s-gitlab-pages-site.conf", generateUniqueNameFromGitlabPagesSite(gitlabPagesSite))
}

type templateData struct {
	Hosts     []string
	Anonymous bool

	GitlabPagesURL string
	OIDCReturnPath string
	OIDCSecret     struct {
		ClientID, ClientSecret, IssuerURL, SuggestedCookieSecret string
	}
}

func generateHttpdVhostConfiguration(gitlabPagesSite *webservicescernchv1alpha1.GitlabPagesSite, oidcSecret *v1.Secret, gitlabPagesURL, oidcReturnPath string) ([]byte, error) {
	var buf bytes.Buffer

	data := templateData{
		Hosts:          gitlabPagesSite.Spec.Hosts.ToStringSlice(),
		Anonymous:      gitlabPagesSite.Spec.Anonymous,
		GitlabPagesURL: gitlabPagesURL,
		OIDCReturnPath: oidcReturnPath,
	}

	data.OIDCSecret.ClientID = string(oidcSecret.Data["clientID"])
	data.OIDCSecret.ClientSecret = string(oidcSecret.Data["clientSecret"])
	data.OIDCSecret.IssuerURL = string(oidcSecret.Data["issuerURL"])
	data.OIDCSecret.SuggestedCookieSecret = string(oidcSecret.Data["suggestedCookieSecret"])

	// make sure that required values are not empty
	if data.OIDCSecret.ClientID == "" {
		return []byte(""), errors.New("missing OIDC client id")
	} else if data.OIDCSecret.ClientSecret == "" {
		return []byte(""), errors.New("missing OIDC client secret")
	} else if data.OIDCSecret.IssuerURL == "" {
		return []byte(""), errors.New("missing OIDC issuer URL")
	} else if data.OIDCSecret.SuggestedCookieSecret == "" {
		return []byte(""), errors.New("missing suggested cookie secret")
	}

	if err := httpdTemplate.Execute(&buf, data); err != nil {
		return []byte(""), err
	}

	return buf.Bytes(), nil
}

/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"regexp"
	"strconv"
	"strings"

	"github.com/go-logr/logr"
	routev1 "github.com/openshift/api/route/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	authzalpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	webservicescernchv1alpha1 "gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/api/v1alpha1"
)

const gitlabPagesSiteFinalizer string = "staticsites.webservices.cern.ch/static-site-finalizer"
const gitlabPagesSiteRouteOwnershipLabel = "staticsites.webservices.cern.ch/static-site-owner"
const gitlabPagesSiteRouteOwnershipAnnotation = "staticsites.webservices.cern.ch/static-site-owner"
const userNamespaceLabel = "okd.cern.ch/user-project"

// namespace annotations to override certain behaviors on sites provisioned from resources in that namespace
// (annotations shared with webeos-site-operator)
const forceRouterShardNamespaceAnnotation = "webeos.webservices.cern.ch/force-router-shard" // assign routes to a specific ingress controller
const forceIpWhitelistNamespaceAnnotation = "webeos.webservices.cern.ch/force-ip-whitelist" // set an IP whitelist on routes (e.g. to block access from Internet)

// GitlabPagesSiteReconciler reconciles a GitlabPagesSite object
type GitlabPagesSiteReconciler struct {
	client.Client
	Scheme                       *runtime.Scheme
	Namespace                    string
	VirtualHostsSecretPrefixName string
	GitlabPagesURL               string
	AuthProxyServiceName         string
	AuthProxyServicePort         int
	VirtualHostsSecretCount      int
	OIDCReturnPath               string
	RouterShardLabel             string
	DefaultRouterShard           string
	SharedSubdomainRegex         *regexp.Regexp
	logger                       logr.Logger
}

//+kubebuilder:rbac:groups=staticsites.webservices.cern.ch,resources=gitlabpagessites,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=staticsites.webservices.cern.ch,resources=gitlabpagessites/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=staticsites.webservices.cern.ch,resources=gitlabpagessites/finalizers,verbs=update
//+kubebuilder:rbac:groups=webservices.cern.ch,resources=applicationregistrations,verbs=get;list;watch
//+kubebuilder:rbac:groups=webservices.cern.ch,resources=oidcreturnuris,verbs=get;list;watch;create;patch;update;delete
//+kubebuilder:rbac:groups=route.openshift.io,resources=routes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=route.openshift.io,resources=routes/custom-host,verbs=create;get;update;patch
//+kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;update;patch
//+kubebuilder:rbac:groups="",resources=namespaces,verbs=get;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the GitlabPagesSite object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.8.3/pkg/reconcile
func (r *GitlabPagesSiteReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	r.logger = log.Log.WithValues("GitlabPagesSite", req.NamespacedName)
	r.logger.Info("Reconciling GitlabPagesSite")

	gitlabPagesSite := &webservicescernchv1alpha1.GitlabPagesSite{}
	if err := r.Get(ctx, req.NamespacedName, gitlabPagesSite); err != nil {
		if errors.IsNotFound(err) {
			r.logger.Info("GitlabPagesSite resource not found. Ignoring reconciliation request since object must have been deleted.")
			return ctrl.Result{}, nil
		}

		r.logger.Error(err, "Failed to get GitlabPagesSite")
		return ctrl.Result{}, err
	}

	if !controllerutil.ContainsFinalizer(gitlabPagesSite, gitlabPagesSiteFinalizer) {
		controllerutil.AddFinalizer(gitlabPagesSite, gitlabPagesSiteFinalizer)

		err := r.Update(ctx, gitlabPagesSite, &client.UpdateOptions{})
		if err != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	// Secret holding the httpd configuration
	httpdVHostSecret := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: r.Namespace,
			Name:      getHttpdVHostSecretName(r.VirtualHostsSecretPrefixName, gitlabPagesSite.Name, r.VirtualHostsSecretCount),
		},
	}

	// GitlabPagesSite has been deleted - we should handle the deletion of the entry from the Secret
	if gitlabPagesSite.GetDeletionTimestamp() != nil {
		r.logger.Info("Deleting GitlabPagesSite")

		if controllerutil.ContainsFinalizer(gitlabPagesSite, gitlabPagesSiteFinalizer) {
			routes, err := r.getRoutes(ctx, gitlabPagesSite)

			if err != nil {
				if !errors.IsNotFound(err) {
					return ctrl.Result{}, err
				}
			} else {
				for _, route := range routes.Items {
					err = r.Delete(ctx, &route, &client.DeleteOptions{})
					if err != nil {
						return ctrl.Result{}, err
					}
				}
			}

			// delete the entry from the httpd config (stored in a Secret)
			_, err = controllerutil.CreateOrUpdate(ctx, r.Client, httpdVHostSecret, func() error {
				delete(httpdVHostSecret.Data, generateSecretKey(gitlabPagesSite))
				return nil
			})

			if err != nil {
				return ctrl.Result{}, err
			}

			controllerutil.RemoveFinalizer(gitlabPagesSite, gitlabPagesSiteFinalizer)
			err = r.Update(ctx, gitlabPagesSite, &client.UpdateOptions{})
			if err != nil {
				return ctrl.Result{}, err
			}
		}

		r.logger.Info("GitlabPagesSite has been deleted")
		return ctrl.Result{}, nil
	}

	oidcSecret, err := r.getOidcSecret(ctx, gitlabPagesSite)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.ensureGitlabPagesSiteHttpdConfig(ctx, gitlabPagesSite, httpdVHostSecret, oidcSecret)
	if err != nil {
		r.logger.Error(err, "There was a problem while generating httpd config for the GitlabPagesSite for "+gitlabPagesSite.Name+" "+gitlabPagesSite.Namespace+" in "+getHttpdVHostSecretName(r.VirtualHostsSecretPrefixName, gitlabPagesSite.Name, r.VirtualHostsSecretCount))
		return ctrl.Result{}, err
	}

	err = r.ensureRoutes(ctx, gitlabPagesSite)
	if err != nil {
		r.logger.Error(err, "There was a problem while creating a Route")
		return ctrl.Result{}, err
	}

	// Ensure that a OidcReturnURI is either created or updated
	if err := r.ensureOIDCReturnURIs(ctx, gitlabPagesSite); err != nil {
		r.logger.Error(err, "Error creating/updating OidcReturnURI")
		return ctrl.Result{}, err
	}

	if !meta.IsStatusConditionPresentAndEqual(gitlabPagesSite.Status.Conditions, webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated, metav1.ConditionTrue) {
		meta.SetStatusCondition(&gitlabPagesSite.Status.Conditions, metav1.Condition{
			Type:    webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated,
			Status:  metav1.ConditionTrue,
			Reason:  webservicescernchv1alpha1.ConditionReasonSuccessful,
			Message: "Awaiting next reconciliation",
		})

		if err := r.Status().Update(ctx, gitlabPagesSite); err != nil {
			r.logger.Error(err, "Failed to update GitlabPagesSite Status")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// getHttpdVHostSecretName generates a consistent secret name for the httpd configuration
//
// Parameters:
//   - prefix (string): The prefix for the secret name.
//   - website (string): The input string for which the consistent integer value is computed.
//   - limit (int): The upper bound (inclusive) of the range for the returned value.
//
// Returns:
//   - name: A consistent secret name formed by prefix-{inteber between 0 and limit}.
//
func getHttpdVHostSecretName(prefix string, website string, limit int) string {
	if limit <= 0 {
		panic("max must be greater than 0")
	}
	sum := 0
	for _, char := range website {
		sum += int(char)
	}
	value := sum % limit

	return strings.Join([]string{prefix, "-", strconv.Itoa(value)}, "")
}

// SetupWithManager sets up the controller with the Manager.
func (r *GitlabPagesSiteReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webservicescernchv1alpha1.GitlabPagesSite{}).
		Owns(&authzalpha1.OidcReturnURI{}).
		// Reconcile GitlabPagesSite if its Route is deleted/changed (We only care about Route in r.Namespace with a specific annotation)
		Watches(&source.Kind{Type: &routev1.Route{}}, handler.EnqueueRequestsFromMapFunc(
			func(obj client.Object) []reconcile.Request {
				namespace := obj.GetNamespace()

				// we only care about the `Route`s within the namespace specified by r.Namespace
				if namespace != r.Namespace {
					return []reconcile.Request{}
				}

				annotations := obj.GetAnnotations()
				if annotations != nil && annotations[gitlabPagesSiteRouteOwnershipAnnotation] != "" {
					parts := strings.Split(annotations[gitlabPagesSiteRouteOwnershipAnnotation], "/")
					return []reconcile.Request{{NamespacedName: types.NamespacedName{Name: parts[1], Namespace: parts[0]}}}
				}

				return []reconcile.Request{}
			}),
		).
		// Reconcile GitlabPagesSite(s) if labels / annotations on the parent namespace changes
		Watches(&source.Kind{Type: &v1.Namespace{}}, handler.EnqueueRequestsFromMapFunc(
			func(a client.Object) []reconcile.Request {
				requests := []reconcile.Request{}
				_, exists := a.GetLabels()[userNamespaceLabel]
				if exists {
					sites, err := r.getGitlabPagesSitesInNamespace(context.TODO(), a.GetName())
					if err != nil {
						r.logger.Error(err, "Failed to retrieve GitlabPagesSites in namespace "+a.GetName())
						return requests
					}
					// perform operator sync for each GPS site we found
					for _, site := range sites.Items {
						requests = append(requests, reconcile.Request{NamespacedName: types.NamespacedName{Name: site.Name, Namespace: site.Namespace}})
					}
				}
				return requests
			}),
		).
		Complete(r)
}

package controllers

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	routev1 "github.com/openshift/api/route/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	authzalpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	webservicescernchv1alpha1 "gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"

	"k8s.io/apimachinery/pkg/types"
)

func newAppRegistration(name, namespace, secretName string) *authzalpha1.ApplicationRegistration {
	return &authzalpha1.ApplicationRegistration{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: authzalpha1.ApplicationRegistrationSpec{
			ApplicationName: "app-name",
			Description:     "app-description",
			InitialOwner:    authzalpha1.InitialOwner{Username: "app-owner"},
		},
		Status: authzalpha1.ApplicationRegistrationStatus{
			ClientCredentialsSecret: secretName,
			ProvisioningStatus:      "Created",
		},
	}
}

func newOidcSecret(name, namespace string) *corev1.Secret {
	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		StringData: map[string]string{
			"clientID":              "gps-client-id",
			"clientSecret":          "gps-client-secret",
			"issuerURL":             "https://foo.gps.cern.ch",
			"suggestedCookieSecret": "gps-suggested-cookie-secret",
		},
	}
}

func newGpsSite(name, namespace string) *webservicescernchv1alpha1.GitlabPagesSite {
	return &webservicescernchv1alpha1.GitlabPagesSite{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: webservicescernchv1alpha1.GitlabPagesSiteSpec{},
	}
}

var _ = Describe("GitlabPagesSite controller", func() {
	// Define utility constants for object names and testing timeouts/durations and intervals.
	const (
		gpsName        = "gps-name"
		gpsNamespace   = "gps-namespace"
		oidcSecretName = "gps-test-secret"

		timeout  = time.Second * 10
		duration = time.Second * 10
		interval = time.Millisecond * 500
	)

	var gpsNs *corev1.Namespace
	var appRegistration *authzalpha1.ApplicationRegistration
	var oidcSecret *corev1.Secret
	var ctx context.Context = context.Background()
	var gpsLookupKey = types.NamespacedName{Name: gpsName, Namespace: gpsNamespace}
	var gpsUrls = webservicescernchv1alpha1.SiteUrls{"foo2.web.cern.ch", "foo3.docs.cern.ch"}

	BeforeEach(func() {
		gpsNs = &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: gpsNamespace,
			},
		}

		// deleting a namespace will not work since the testenv doesn't have controllers
		// that could reconcile standard kubernetes objects
		_, err := controllerutil.CreateOrUpdate(ctx, k8sClient, gpsNs, func() error { return nil })
		Expect(err).Should(Succeed())
		Expect(gpsNs.Status.Phase).To(Equal(corev1.NamespaceActive))

		appRegistration = newAppRegistration(gpsNamespace, gpsNamespace, oidcSecretName)
		Expect(k8sClient.Create(ctx, appRegistration)).Should(Succeed())

		oidcSecret = newOidcSecret(oidcSecretName, gpsNamespace)
		Expect(k8sClient.Create(ctx, oidcSecret)).Should(Succeed())
	})

	AfterEach(func() {
		Expect(k8sClient.DeleteAllOf(ctx, &webservicescernchv1alpha1.GitlabPagesSite{}, client.InNamespace(gpsNamespace))).Should(Succeed())
		Expect(k8sClient.Delete(ctx, oidcSecret)).Should(Succeed())
		Expect(k8sClient.Delete(ctx, appRegistration)).Should(Succeed())
		Expect(k8sClient.DeleteAllOf(ctx, &authzalpha1.OidcReturnURI{}, client.InNamespace(gpsNamespace))).Should(Succeed())
	})

	Context("When creating a GitlabPagesSite", func() {
		It("Should make sure that .Hosts can't be an empty slice", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{}

			Expect(k8sClient.Create(ctx, gpsSite)).Should(HaveOccurred())

			gpsSite.Spec.Hosts = gpsUrls
			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
		})

		It("Should make sure all necessary resources have been created for an SSO-protected site and its Status has been updated", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = gpsUrls

			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			vHostSecretName := getHttpdVHostSecretName(vHostsSecretPrefix, gpsSite.Name, vHostsSecretCount)
			httpdSecret := &corev1.Secret{}
			err := k8sClient.Get(ctx, types.NamespacedName{Name: vHostSecretName, Namespace: operatorNamespace}, httpdSecret)

			httpdConfiguration := string(httpdSecret.Data[generateSecretKey(gpsSite)])
			Expect(err).ShouldNot(HaveOccurred())
			Expect(httpdConfiguration).ToNot(BeNil())
			Expect(len(httpdConfiguration)).To(BeNumerically(">", 0))
			Expect(httpdConfiguration).To(ContainSubstring(fmt.Sprintf("ServerName \"%s\"", gpsUrls.ToStringSlice()[0])))
			Expect(httpdConfiguration).To(ContainSubstring(fmt.Sprintf("ServerName \"%s\"", gpsUrls.ToStringSlice()[1])))
			Expect(httpdConfiguration).To(ContainSubstring("OIDCProviderMetadataURL \"https://foo.gps.cern.ch/.well-known/openid-configuration\""))
			Expect(httpdConfiguration).To(ContainSubstring("OIDCClientID \"gps-client-id\""))
			Expect(httpdConfiguration).To(ContainSubstring("OIDCClientSecret \"gps-client-secret\""))
			Expect(httpdConfiguration).To(ContainSubstring("OIDCCryptoPassphrase \"gps-suggested-cookie-secret\""))

			oidcReturnUris := &authzalpha1.OidcReturnURIList{}
			Expect(k8sClient.List(ctx, oidcReturnUris, client.InNamespace(gpsNamespace))).ToNot(HaveOccurred())
			Expect(len(oidcReturnUris.Items)).To(Equal(2))

			routes := &routev1.RouteList{}
			labelSelector := &metav1.LabelSelector{MatchLabels: map[string]string{gitlabPagesSiteRouteOwnershipLabel: generateOwnershipLabelValue(gpsSite)}}
			listOptions := &client.ListOptions{Namespace: operatorNamespace, LabelSelector: labels.Set(labelSelector.MatchLabels).AsSelector()}

			Expect(k8sClient.List(ctx, routes, listOptions)).ToNot(HaveOccurred())
			Expect(len(routes.Items)).To(Equal(2))

			routeHosts := []string{}
			for _, route := range routes.Items {
				routeHosts = append(routeHosts, route.Spec.Host)
			}

			sort.Strings(routeHosts)
			Expect(routeHosts).To(Equal(gpsUrls.ToStringSlice()))
		})

		It("Should make sure all necessary resources have been created for a publicly available site and its Status has been updated", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = gpsUrls
			gpsSite.Spec.Anonymous = true

			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			vHostSecretName := getHttpdVHostSecretName(vHostsSecretPrefix, gpsSite.Name, vHostsSecretCount)
			httpdSecret := &corev1.Secret{}
			err := k8sClient.Get(ctx, types.NamespacedName{Name: vHostSecretName, Namespace: operatorNamespace}, httpdSecret)

			httpdConfiguration := string(httpdSecret.Data[generateSecretKey(gpsSite)])
			Expect(err).ShouldNot(HaveOccurred())
			Expect(httpdConfiguration).ToNot(BeNil())
			Expect(len(httpdConfiguration)).To(BeNumerically(">", 0))
			Expect(httpdConfiguration).To(ContainSubstring(fmt.Sprintf("ServerName \"%s\"", gpsUrls.ToStringSlice()[0])))
			Expect(httpdConfiguration).To(ContainSubstring(fmt.Sprintf("ServerName \"%s\"", gpsUrls.ToStringSlice()[1])))
			Expect(httpdConfiguration).ToNot(ContainSubstring("OIDCProviderMetadataURL \"https://foo.gps.cern.ch/.well-known/openid-configuration\""))
			Expect(httpdConfiguration).ToNot(ContainSubstring("OIDCClientID \"gps-client-id\""))
			Expect(httpdConfiguration).ToNot(ContainSubstring("OIDCClientSecret \"gps-client-secret\""))
			Expect(httpdConfiguration).ToNot(ContainSubstring("OIDCCryptoPassphrase \"gps-suggested-cookie-secret\""))

			oidcReturnUris := &authzalpha1.OidcReturnURIList{}
			Expect(k8sClient.List(ctx, oidcReturnUris, client.InNamespace(gpsNamespace))).ToNot(HaveOccurred())
			Expect(len(oidcReturnUris.Items)).To(Equal(2))

			routes := &routev1.RouteList{}
			labelSelector := &metav1.LabelSelector{MatchLabels: map[string]string{gitlabPagesSiteRouteOwnershipLabel: generateOwnershipLabelValue(gpsSite)}}
			listOptions := &client.ListOptions{Namespace: operatorNamespace, LabelSelector: labels.Set(labelSelector.MatchLabels).AsSelector()}

			Expect(k8sClient.List(ctx, routes, listOptions)).ToNot(HaveOccurred())
			Expect(len(routes.Items)).To(Equal(2))

			routeHosts := []string{}
			for _, route := range routes.Items {
				routeHosts = append(routeHosts, route.Spec.Host)
			}

			sort.Strings(routeHosts)
			Expect(routeHosts).To(Equal(gpsUrls.ToStringSlice()))
		})

		It("Removing an item from Hosts should delete corresponding Route and OIDCReturnURI", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = gpsUrls

			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			oidcReturnUris := &authzalpha1.OidcReturnURIList{}
			Expect(k8sClient.List(ctx, oidcReturnUris, client.InNamespace(gpsNamespace))).ToNot(HaveOccurred())
			Expect(len(oidcReturnUris.Items)).To(Equal(2))

			routes := &routev1.RouteList{}
			labelSelector := &metav1.LabelSelector{MatchLabels: map[string]string{gitlabPagesSiteRouteOwnershipLabel: generateOwnershipLabelValue(gpsSite)}}
			listOptions := &client.ListOptions{Namespace: operatorNamespace, LabelSelector: labels.Set(labelSelector.MatchLabels).AsSelector()}

			Expect(k8sClient.List(ctx, routes, listOptions)).ToNot(HaveOccurred())
			Expect(len(routes.Items)).To(Equal(2))

			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{"foo.web.cern.ch"}
			Expect(k8sClient.Update(ctx, gpsSite)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.List(ctx, oidcReturnUris, client.InNamespace(gpsNamespace))
				return err == nil && len(oidcReturnUris.Items) == 1 && strings.Contains(oidcReturnUris.Items[0].Spec.RedirectURI, "foo.web.cern.ch")
			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				err := k8sClient.List(ctx, routes, listOptions)
				return err == nil && len(routes.Items) == 1 && strings.Contains(routes.Items[0].Spec.Host, "foo.web.cern.ch")
			}, timeout, interval).Should(BeTrue())
		})

		It("Routes for hosts that don't match specific regex will be configured with support for automatic certificate provisioning with openshift-acme", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{"foo.cern.ch", "bar.web.cern.ch"}

			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			routes := &routev1.RouteList{}
			labelSelector := &metav1.LabelSelector{MatchLabels: map[string]string{gitlabPagesSiteRouteOwnershipLabel: generateOwnershipLabelValue(gpsSite)}}
			listOptions := &client.ListOptions{Namespace: operatorNamespace, LabelSelector: labels.Set(labelSelector.MatchLabels).AsSelector()}

			Expect(k8sClient.List(ctx, routes, listOptions)).ToNot(HaveOccurred())
			Expect(len(routes.Items)).To(Equal(2))

			for _, route := range routes.Items {
				if route.Spec.Host == "foo.cern.ch" {
					Expect(route.Annotations).Should(HaveKeyWithValue("kubernetes.io/tls-acme", "true"))
				} else if route.Spec.Host == "bar.web.cern.ch" {
					Expect(route.Annotations).ShouldNot(HaveKeyWithValue("kubernetes.io/tls-acme", "true"))
				}
			}
		})

		It("GPS sites in blocked projects are unavailable", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{"my-gps-site.web.cern.ch"}

			// mark the namespace in which this GPS site exists as "blocked"
			gpsNs := &corev1.Namespace{}
			nsKey := types.NamespacedName{Name: gpsNamespace, Namespace: gpsNamespace}
			Expect(k8sClient.Get(ctx, nsKey, gpsNs)).Should(Succeed())
			if gpsNs.Labels == nil {
				gpsNs.Labels = map[string]string{}
			}
			gpsNs.Labels[projectBlockedLabel] = "true"
			Expect(k8sClient.Update(ctx, gpsNs)).Should(Succeed())

			// create a GPS site as usual
			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			// confirm that the routes do *NOT* point to the gitlab-pages-proxy deployment
			// (but the route should still exist)
			routes := &routev1.RouteList{}
			labelSelector := &metav1.LabelSelector{MatchLabels: map[string]string{gitlabPagesSiteRouteOwnershipLabel: generateOwnershipLabelValue(gpsSite)}}
			listOptions := &client.ListOptions{Namespace: operatorNamespace, LabelSelector: labels.Set(labelSelector.MatchLabels).AsSelector()}

			Expect(k8sClient.List(ctx, routes, listOptions)).ToNot(HaveOccurred())
			Expect(len(routes.Items)).To(Equal(1))

			for _, route := range routes.Items {
				Expect(route.Spec.To.Name).To(Equal(blockedServiceName))
			}
		})

		It("GPS's Route is assigned to the default router shard", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{"my-gps-site.web.cern.ch"}

			// create a GPS site as usual
			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			route := &routev1.Route{}
			routeName := generateRouteName(gpsSite, "my-gps-site.web.cern.ch")

			Expect(k8sClient.Get(ctx, types.NamespacedName{Name: routeName, Namespace: operatorNamespace}, route)).Should(Succeed())
			Expect(route.Labels[routerShardLabel]).To(Equal(defaultRouterShard))
		})

		It("GPS's Route is assigned to a custom router shard if configured properly", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{"my-gps-site.web.cern.ch"}

			gpsNs := &corev1.Namespace{}
			nsKey := types.NamespacedName{Name: gpsNamespace, Namespace: gpsNamespace}

			Expect(k8sClient.Get(ctx, nsKey, gpsNs)).Should(Succeed())

			if gpsNs.Annotations == nil {
				gpsNs.Annotations = map[string]string{}
			}

			gpsNs.Annotations[forceRouterShardNamespaceAnnotation] = "example-router-shard"

			Expect(k8sClient.Update(ctx, gpsNs)).Should(Succeed())

			// create a GPS site as usual
			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			// confirm that the proper shard-related labels/annotations have been applied on the Route
			route := &routev1.Route{}
			routeName := generateRouteName(gpsSite, "my-gps-site.web.cern.ch")

			Expect(k8sClient.Get(ctx, types.NamespacedName{Name: routeName, Namespace: operatorNamespace}, route)).Should(Succeed())
			Expect(route.Labels[routerShardLabel]).To(Equal("example-router-shard"))
		})

		It("Namespace's IP whitelist is propagated to the Route", func() {
			gpsSite := newGpsSite(gpsName, gpsNamespace)
			gpsSite.Spec.Hosts = webservicescernchv1alpha1.SiteUrls{"my-gps-site.web.cern.ch"}

			gpsNs := &corev1.Namespace{}
			nsKey := types.NamespacedName{Name: gpsNamespace, Namespace: gpsNamespace}

			Expect(k8sClient.Get(ctx, nsKey, gpsNs)).Should(Succeed())

			if gpsNs.Annotations == nil {
				gpsNs.Annotations = map[string]string{}
			}

			gpsNs.Annotations[forceIpWhitelistNamespaceAnnotation] = "example-ip-whitelist"

			Expect(k8sClient.Update(ctx, gpsNs)).Should(Succeed())

			// create a GPS site as usual
			Expect(k8sClient.Create(ctx, gpsSite)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gpsLookupKey, gpsSite)
				if err != nil {
					return false
				}

				return len(gpsSite.Status.Conditions) == 1 &&
					gpsSite.Status.Conditions[0].Type == webservicescernchv1alpha1.ConditionTypeGitlabPagesSiteCreated &&
					gpsSite.Status.Conditions[0].Status == metav1.ConditionTrue
			}, timeout, interval).Should(BeTrue())

			route := &routev1.Route{}
			routeName := generateRouteName(gpsSite, "my-gps-site.web.cern.ch")

			Expect(k8sClient.Get(ctx, types.NamespacedName{Name: routeName, Namespace: operatorNamespace}, route)).Should(Succeed())
			Expect(route.Annotations["haproxy.router.openshift.io/ip_whitelist"]).To(Equal("example-ip-whitelist"))
		})

		It("Create a secret name that matches prefix + a random number never bigger than limit", func() {
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website", 1)).To(Equal("secret-prefix-0"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-0", 2)).To(Equal("secret-prefix-1"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-1", 2)).To(Equal("secret-prefix-0"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-2", 2)).To(Equal("secret-prefix-1"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-3", 2)).To(Equal("secret-prefix-0"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-4", 2)).To(Equal("secret-prefix-1"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-5", 2)).To(Equal("secret-prefix-0"))
			Expect(getHttpdVHostSecretName("secret-prefix", "my-website-6", 2)).To(Equal("secret-prefix-1"))
		})
	})
})

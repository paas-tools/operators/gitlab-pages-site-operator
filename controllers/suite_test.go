/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"path/filepath"
	"regexp"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	routev1 "github.com/openshift/api/route/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	"sigs.k8s.io/controller-runtime/pkg/envtest/printer"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	authzalpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	webservicescernchv1alpha1 "gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/api/v1alpha1"
	//+kubebuilder:scaffold:imports
)

// const variables used to initialize the Reconciler (they are also needed in the controller_test.go)
const (
	operatorNamespace    = "default"
	vHostsSecretPrefix   = "httpd-secret"
	vHostsSecretCount    = 10
	authProxyServiceName = "service-name"
	authProxyServicePort = 8080
	routerShardLabel     = "selected-router-shard-label"
	defaultRouterShard   = "default-router-shard"
	oidcReturnPath       = "/oidc-return-path"
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

var k8sClient client.Client
var testEnv *envtest.Environment
var ctx context.Context
var cancel context.CancelFunc

var crds []client.Object = []client.Object{
	&apiextensionsv1.CustomResourceDefinition{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "apiextensions.k8s.io/v1",
			Kind:       "CustomResourceDefinition",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "oidcreturnuris.webservices.cern.ch",
		},
		Spec: apiextensionsv1.CustomResourceDefinitionSpec{
			Group: "webservices.cern.ch",
			Names: apiextensionsv1.CustomResourceDefinitionNames{
				Plural:   "oidcreturnuris",
				Singular: "oidcreturnuri",
				Kind:     "OidcReturnURI",
			},
			Scope: apiextensionsv1.NamespaceScoped,
			Versions: []apiextensionsv1.CustomResourceDefinitionVersion{
				{
					Name:    "v1alpha1",
					Served:  true,
					Storage: true,
					Schema: &apiextensionsv1.CustomResourceValidation{
						OpenAPIV3Schema: &apiextensionsv1.JSONSchemaProps{
							Type: "object",
							Properties: map[string]apiextensionsv1.JSONSchemaProps{
								"spec": {
									Type: "object",
									Properties: map[string]apiextensionsv1.JSONSchemaProps{
										"redirectURI": {
											Type: "string",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
	&apiextensionsv1.CustomResourceDefinition{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "apiextensions.k8s.io/v1",
			Kind:       "CustomResourceDefinition",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "routes.route.openshift.io",
		},
		Spec: apiextensionsv1.CustomResourceDefinitionSpec{
			Group: "route.openshift.io",
			Names: apiextensionsv1.CustomResourceDefinitionNames{
				Plural:   "routes",
				Singular: "route",
				Kind:     "Route",
			},
			Scope: apiextensionsv1.NamespaceScoped,
			Versions: []apiextensionsv1.CustomResourceDefinitionVersion{
				{
					Name:    "v1",
					Served:  true,
					Storage: true,
					Schema: &apiextensionsv1.CustomResourceValidation{
						OpenAPIV3Schema: &apiextensionsv1.JSONSchemaProps{
							Type: "object",
							Properties: map[string]apiextensionsv1.JSONSchemaProps{
								"spec": {
									Type: "object",
									Properties: map[string]apiextensionsv1.JSONSchemaProps{
										"host": {
											Type: "string",
										},
										"tls": {
											Type: "object",
											Properties: map[string]apiextensionsv1.JSONSchemaProps{
												"insecureEdgeTerminationPolicy": {
													Type: "string",
												},
												"termination": {
													Type: "string",
												},
											},
										},
										"to": {
											Type: "object",
											Properties: map[string]apiextensionsv1.JSONSchemaProps{
												"name": {
													Type: "string",
												},
												"kind": {
													Type: "string",
												},
											},
										},
										"port": {
											Type: "object",
											Properties: map[string]apiextensionsv1.JSONSchemaProps{
												"targetPort": {
													Type: "integer",
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
	&apiextensionsv1.CustomResourceDefinition{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "apiextensions.k8s.io/v1",
			Kind:       "CustomResourceDefinition",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "applicationregistrations.webservices.cern.ch",
		},
		Spec: apiextensionsv1.CustomResourceDefinitionSpec{
			Group: "webservices.cern.ch",
			Names: apiextensionsv1.CustomResourceDefinitionNames{
				Plural:   "applicationregistrations",
				Singular: "applicationregistration",
				Kind:     "ApplicationRegistration",
			},
			Scope: apiextensionsv1.NamespaceScoped,
			Versions: []apiextensionsv1.CustomResourceDefinitionVersion{
				{
					Name:    "v1alpha1",
					Served:  true,
					Storage: true,
					Schema: &apiextensionsv1.CustomResourceValidation{
						OpenAPIV3Schema: &apiextensionsv1.JSONSchemaProps{
							Type: "object",
							Properties: map[string]apiextensionsv1.JSONSchemaProps{
								"status": {
									Type: "object",
									Properties: map[string]apiextensionsv1.JSONSchemaProps{
										"clientCredentialsSecret": {
											Type: "string",
										},
										"provisioningStatus": {
											Type: "string",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
}

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecsWithDefaultAndCustomReporters(t,
		"Controller Suite",
		[]Reporter{printer.NewlineReporter{}})
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.TODO())

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "chart", "crds")},
		ErrorIfCRDPathMissing: true,
		CRDs:                  crds,
	}

	cfg, err := testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	err = webservicescernchv1alpha1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	//+kubebuilder:scaffold:scheme

	err = authzalpha1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = routev1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	k8sManager, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme: scheme.Scheme,
	})
	Expect(err).ToNot(HaveOccurred())

	domainRegex, err := regexp.Compile(`[^.]+\.(web.cern.ch|docs.cern.ch)`)
	Expect(err).ToNot(HaveOccurred())

	err = (&GitlabPagesSiteReconciler{
		Client:                       k8sManager.GetClient(),
		Scheme:                       k8sManager.GetScheme(),
		Namespace:                    operatorNamespace,
		VirtualHostsSecretPrefixName: vHostsSecretPrefix,
		VirtualHostsSecretCount:      vHostsSecretCount,
		AuthProxyServiceName:         authProxyServiceName,
		AuthProxyServicePort:         authProxyServicePort,
		RouterShardLabel:             routerShardLabel,
		DefaultRouterShard:           defaultRouterShard,
		OIDCReturnPath:               oidcReturnPath,
		SharedSubdomainRegex:         domainRegex,
	}).SetupWithManager(k8sManager)
	Expect(err).ToNot(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(ctx)
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()
}, 60)

var _ = AfterSuite(func() {
	cancel()
	By("tearing down the test environment")
	err := testEnv.Stop()
	Expect(err).NotTo(HaveOccurred())
})

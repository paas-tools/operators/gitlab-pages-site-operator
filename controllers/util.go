package controllers

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/url"

	webservicescernchv1alpha1 "gitlab.cern.ch/paas-tools/operators/gitlab-pages-site-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
)

const projectBlockedLabel = "okd.cern.ch/project-blocked"

func getOIDCReturnUri(domain, oidcReturnPath string) string {
	uri := url.URL{
		Scheme: "https",
		Host:   domain,
		Path:   oidcReturnPath,
	}

	return uri.String()
}

func generateUniqueNameFromGitlabPagesSite(gitlabPagesSite *webservicescernchv1alpha1.GitlabPagesSite) string {
	return fmt.Sprintf("%s.%s", gitlabPagesSite.Namespace, gitlabPagesSite.Name)
}

func generateHostHash(host string) string {
	hash := md5.Sum([]byte(host))
	return hex.EncodeToString(hash[0:5])
}

func generateRouteName(gitlabPagesSite *webservicescernchv1alpha1.GitlabPagesSite, host string) string {
	hostHash := generateHostHash(host)
	return fmt.Sprintf("%s-%s", generateUniqueNameFromGitlabPagesSite(gitlabPagesSite), hostHash)
}

func generateOidcReturnUriName(gitlabPagesSite *webservicescernchv1alpha1.GitlabPagesSite, host string) string {
	hostHash := generateHostHash(host)
	return fmt.Sprintf("%s-%s", generateUniqueNameFromGitlabPagesSite(gitlabPagesSite), hostHash)
}

// Label values are limited to be not longer than 63 characters thus gps' UID is used for the value of the ownership label
func generateOwnershipLabelValue(gitlabPagesSite *webservicescernchv1alpha1.GitlabPagesSite) string {
	return string(gitlabPagesSite.UID)
}

func projectBlocked(namespace v1.Namespace) bool {
	value, found := namespace.ObjectMeta.Labels[projectBlockedLabel]
	if found && value == "true" {
		return true
	}
	return false
}

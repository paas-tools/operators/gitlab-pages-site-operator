// +build !ignore_autogenerated

/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1alpha1

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *AppRole) DeepCopyInto(out *AppRole) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new AppRole.
func (in *AppRole) DeepCopy() *AppRole {
	if in == nil {
		return nil
	}
	out := new(AppRole)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApplicationRegistration) DeepCopyInto(out *ApplicationRegistration) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	out.Spec = in.Spec
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApplicationRegistration.
func (in *ApplicationRegistration) DeepCopy() *ApplicationRegistration {
	if in == nil {
		return nil
	}
	out := new(ApplicationRegistration)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApplicationRegistration) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApplicationRegistrationList) DeepCopyInto(out *ApplicationRegistrationList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]ApplicationRegistration, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApplicationRegistrationList.
func (in *ApplicationRegistrationList) DeepCopy() *ApplicationRegistrationList {
	if in == nil {
		return nil
	}
	out := new(ApplicationRegistrationList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApplicationRegistrationList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApplicationRegistrationSpec) DeepCopyInto(out *ApplicationRegistrationSpec) {
	*out = *in
	out.InitialOwner = in.InitialOwner
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApplicationRegistrationSpec.
func (in *ApplicationRegistrationSpec) DeepCopy() *ApplicationRegistrationSpec {
	if in == nil {
		return nil
	}
	out := new(ApplicationRegistrationSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApplicationRegistrationStatus) DeepCopyInto(out *ApplicationRegistrationStatus) {
	*out = *in
	in.TokenExchangePermissions.DeepCopyInto(&out.TokenExchangePermissions)
	if in.RedirectURIs != nil {
		in, out := &in.RedirectURIs, &out.RedirectURIs
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApplicationRegistrationStatus.
func (in *ApplicationRegistrationStatus) DeepCopy() *ApplicationRegistrationStatus {
	if in == nil {
		return nil
	}
	out := new(ApplicationRegistrationStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BootstrapApplicationRole) DeepCopyInto(out *BootstrapApplicationRole) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BootstrapApplicationRole.
func (in *BootstrapApplicationRole) DeepCopy() *BootstrapApplicationRole {
	if in == nil {
		return nil
	}
	out := new(BootstrapApplicationRole)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *BootstrapApplicationRole) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BootstrapApplicationRoleList) DeepCopyInto(out *BootstrapApplicationRoleList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]BootstrapApplicationRole, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BootstrapApplicationRoleList.
func (in *BootstrapApplicationRoleList) DeepCopy() *BootstrapApplicationRoleList {
	if in == nil {
		return nil
	}
	out := new(BootstrapApplicationRoleList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *BootstrapApplicationRoleList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BootstrapApplicationRoleSpec) DeepCopyInto(out *BootstrapApplicationRoleSpec) {
	*out = *in
	if in.LinkedGroups != nil {
		in, out := &in.LinkedGroups, &out.LinkedGroups
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BootstrapApplicationRoleSpec.
func (in *BootstrapApplicationRoleSpec) DeepCopy() *BootstrapApplicationRoleSpec {
	if in == nil {
		return nil
	}
	out := new(BootstrapApplicationRoleSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BootstrapApplicationRoleStatus) DeepCopyInto(out *BootstrapApplicationRoleStatus) {
	*out = *in
	if in.Conditions != nil {
		in, out := &in.Conditions, &out.Conditions
		*out = make([]v1.Condition, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BootstrapApplicationRoleStatus.
func (in *BootstrapApplicationRoleStatus) DeepCopy() *BootstrapApplicationRoleStatus {
	if in == nil {
		return nil
	}
	out := new(BootstrapApplicationRoleStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *InitialOwner) DeepCopyInto(out *InitialOwner) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new InitialOwner.
func (in *InitialOwner) DeepCopy() *InitialOwner {
	if in == nil {
		return nil
	}
	out := new(InitialOwner)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OidcReturnURI) DeepCopyInto(out *OidcReturnURI) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	out.Spec = in.Spec
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OidcReturnURI.
func (in *OidcReturnURI) DeepCopy() *OidcReturnURI {
	if in == nil {
		return nil
	}
	out := new(OidcReturnURI)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *OidcReturnURI) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OidcReturnURIList) DeepCopyInto(out *OidcReturnURIList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]OidcReturnURI, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OidcReturnURIList.
func (in *OidcReturnURIList) DeepCopy() *OidcReturnURIList {
	if in == nil {
		return nil
	}
	out := new(OidcReturnURIList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *OidcReturnURIList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OidcReturnURISpec) DeepCopyInto(out *OidcReturnURISpec) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OidcReturnURISpec.
func (in *OidcReturnURISpec) DeepCopy() *OidcReturnURISpec {
	if in == nil {
		return nil
	}
	out := new(OidcReturnURISpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OidcReturnURIStatus) DeepCopyInto(out *OidcReturnURIStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OidcReturnURIStatus.
func (in *OidcReturnURIStatus) DeepCopy() *OidcReturnURIStatus {
	if in == nil {
		return nil
	}
	out := new(OidcReturnURIStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ProjectLifecyclePolicy) DeepCopyInto(out *ProjectLifecyclePolicy) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	out.Spec = in.Spec
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ProjectLifecyclePolicy.
func (in *ProjectLifecyclePolicy) DeepCopy() *ProjectLifecyclePolicy {
	if in == nil {
		return nil
	}
	out := new(ProjectLifecyclePolicy)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ProjectLifecyclePolicy) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ProjectLifecyclePolicyList) DeepCopyInto(out *ProjectLifecyclePolicyList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]ProjectLifecyclePolicy, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ProjectLifecyclePolicyList.
func (in *ProjectLifecyclePolicyList) DeepCopy() *ProjectLifecyclePolicyList {
	if in == nil {
		return nil
	}
	out := new(ProjectLifecyclePolicyList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ProjectLifecyclePolicyList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ProjectLifecyclePolicySpec) DeepCopyInto(out *ProjectLifecyclePolicySpec) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ProjectLifecyclePolicySpec.
func (in *ProjectLifecyclePolicySpec) DeepCopy() *ProjectLifecyclePolicySpec {
	if in == nil {
		return nil
	}
	out := new(ProjectLifecyclePolicySpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ProjectLifecyclePolicyStatus) DeepCopyInto(out *ProjectLifecyclePolicyStatus) {
	*out = *in
	if in.Conditions != nil {
		in, out := &in.Conditions, &out.Conditions
		*out = make([]v1.Condition, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ProjectLifecyclePolicyStatus.
func (in *ProjectLifecyclePolicyStatus) DeepCopy() *ProjectLifecyclePolicyStatus {
	if in == nil {
		return nil
	}
	out := new(ProjectLifecyclePolicyStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *TokenExchangePermissions) DeepCopyInto(out *TokenExchangePermissions) {
	*out = *in
	if in.Requests != nil {
		in, out := &in.Requests, &out.Requests
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	if in.Allowed != nil {
		in, out := &in.Allowed, &out.Allowed
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new TokenExchangePermissions.
func (in *TokenExchangePermissions) DeepCopy() *TokenExchangePermissions {
	if in == nil {
		return nil
	}
	out := new(TokenExchangePermissions)
	in.DeepCopyInto(out)
	return out
}

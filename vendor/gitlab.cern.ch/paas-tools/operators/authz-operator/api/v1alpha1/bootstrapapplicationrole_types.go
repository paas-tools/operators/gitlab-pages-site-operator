/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Strings for BootstrapApplicationRoleStatus.Conditions
const (
	// Condition Types
	ConditionTypeRoleCreation string = "RoleCreated"
	// Conditions
	ConditionRoleBootstrappedSuccessfully string = "RoleBootstrappedSuccessfully"
	ConditionRoleCreating                 string = "RoleCreating"
	ConditionRoleAlreadyExists            string = "RoleAlreadyExists"
	ConditionRoleCreationError            string = "RoleCreationError"
	ConditionGroupLinkError               string = "GroupLinkError"
	ConditionWaitingForLinkedGroups       string = "WaitingForLinkedGroups"
)

// List of Messages for Status
const (
	StatusMessageCreatedSuccesfully        string = "Created succesfully"
	StatusMessageAlreadyExists             string = "Already existed"
	StatusMessageMissingGroups             string = "Missing Groups: "
	StatusMessageGroupLinkError            string = "Failed to link the following Groups: "
	StatusMessageWaitingNextReconciliation string = "Awaiting next reconciliation"
)

// BootstrapApplicationRoleSpec defines the desired state of BootstrapApplicationRole
type BootstrapApplicationRoleSpec struct {
	// The equivalent of "Role Identifier" of the Role in the Application Portal
	// This field will be
	// The Name field must start with a lowercase letter, can contain only lowercase letters, numbers, dashes and underscores, and must be between 3 and 64 characters long.
	// +kubebuilder:validation:Required
	Name string `json:"name"`

	// DisplayName represents the "Role Name" of the Role in the Application Portal
	// This field is required in order for the Role to be created
	// +kubebuilder:validation:Required
	DisplayName string `json:"displayName"`

	// Description of the ApplicationRegistration, represents the "Description" in the Application Portal
	// Brief description, required for Role creation
	// +kubebuilder:validation:Required
	Description string `json:"description"`

	// Flag to know if the role is required to access the Application
	// +kubebuilder:validation:Required
	RoleRequired bool `json:"required"`

	// MultifactorRequired allows to enable multifactor authentication
	// (From Application Portal): If checked, users must authenticate with Multifactor Authentication to be granted this role
	// +kubebuilder:validation:Required
	MultifactorRequired bool `json:"multifactorRequired"`

	// (From Application Portal): if checked, this role will applied to all authenticated users, regardless of them belonging to any group or not.
	// Use this option to define a role that is based only on the value of the Minimum Level of Assurance and/or usage of Multifactor authentication.
	// +kubebuilder:validation:Required
	ApplyToAllUsers bool `json:"applyToAllUsers"`

	// Level of assurance defines the accepted authentication providers, ranging from CERN identities (highest) to social accounts (public, therefore lowest)
	// Default: 4
	// +kubebuilder:validation:Required
	MinLevelOfAssurance int `json:"minLevelOfAssurance"`

	// List of CERN Groups that are going to be bound to the created Application Role
	// +kubebuilder:validation:Optional
	LinkedGroups []string `json:"linkedGroups"`
}

// BootstrapApplicationRoleStatus defines the observed state of BootstrapApplicationRole
type BootstrapApplicationRoleStatus struct {
	// +kubebuilder:validation:type=string
	// +optional
	RoleID string `json:"id"`
	// +kubebuilder:validation:type=array
	// +optional
	Conditions []metav1.Condition `json:"conditions,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// BootstrapApplicationRole creates but does not maintain Roles to the existing Application in the same namespace in the AuthzAPI.
// More info: https://gitlab.cern.ch/paas-tools/operators/authz-operator#bootstrapapplicationrole
type BootstrapApplicationRole struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BootstrapApplicationRoleSpec   `json:"spec,omitempty"`
	Status BootstrapApplicationRoleStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// BootstrapApplicationRoleList contains a list of BootstrapApplicationRole
type BootstrapApplicationRoleList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []BootstrapApplicationRole `json:"items"`
}

func init() {
	SchemeBuilder.Register(&BootstrapApplicationRole{}, &BootstrapApplicationRoleList{})
}
